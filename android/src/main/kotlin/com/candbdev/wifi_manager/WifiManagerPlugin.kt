package com.candbdev.wifi_manager

import android.content.Context
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.content.IntentFilter
import androidx.core.content.ContextCompat.getSystemService


class WifiManagerPlugin(val registrar: Registrar?, val delegate: WifiManagerDelegate) : MethodCallHandler {

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "wifi_manager")


            val wifiManager = registrar.activeContext().applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val connManager = registrar.activeContext().applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val delegate = WifiManagerDelegate(registrar.activity(), wifiManager, connManager)

            // support Android O,listen network disconnect event
            // https://stackoverflow.com/questions/50462987/android-o-wifimanager-enablenetwork-cannot-work
            val filter = IntentFilter()
            filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            registrar
                    .context()
                    .registerReceiver(delegate.networkReceiver, filter)


            channel.setMethodCallHandler(WifiManagerPlugin(registrar, delegate))
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        if (registrar!!.activity() == null) {
            result.error("no_activity", "wifi plugin requires a foreground activity.", null);
            return
        }

        when {
            call.method == "getPlatformVersion" -> result.success("Android ${android.os.Build.VERSION.RELEASE}")
            call.method == "ssid" -> delegate.getSSID(call, result)
            call.method == "connection" -> delegate.connection(call, result)
            call.method == "startRouting" -> delegate.startRouting(call, result)
            call.method == "stopRouting" -> delegate.stopRouting(call, result)
            else -> result.notImplemented()
        }
    }
}
