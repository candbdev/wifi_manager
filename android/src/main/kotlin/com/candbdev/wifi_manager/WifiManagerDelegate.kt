package com.candbdev.wifi_manager

import android.app.Activity
import android.net.wifi.WifiManager
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.SupplicantState
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiInfo
import android.os.Build


class WifiManagerDelegate(val activity: Activity, val wifiManager: WifiManager, val connectivityManager: ConnectivityManager) {

    //https://stackoverflow.com/questions/36653126/send-request-over-wifi-without-connection-even-if-mobile-data-is-on-with-conn/51469732#51469732
    private var mNetworkCallback: ConnectivityManager.NetworkCallback? = null

    var networkReceiver: NetworkChangeReceiver = NetworkChangeReceiver()

    private var result: MethodChannel.Result? = null
    private var methodCall: MethodCall? = null


    fun getSSID(methodCall: MethodCall, result: MethodChannel.Result) {
        if (!setPendingMethodCallAndResult(methodCall, result)) {
            finishWithAlreadyActiveError()
            return
        }

        launchSSID()
    }

    private fun launchSSID() {
        val wifiName = wifiManager.connectionInfo?.ssid?.replace("\"", "")
        if (!wifiName.isNullOrEmpty()) {
            result!!.success(wifiName)
            clearMethodCallAndResult()
        } else {
            finishWithError("unavailable", "wifi name not available.")
        }
    }


    fun connection(methodCall: MethodCall, result: MethodChannel.Result) {
        if (!setPendingMethodCallAndResult(methodCall, result)) {
            finishWithAlreadyActiveError()
            return
        }
        connection()
    }

    private fun connection() {
        val ssid = methodCall?.argument<String>("ssid")
        val password = methodCall?.argument<String>("password")
        val wifiConfig = createWifiConfig(ssid, password)
        if (wifiConfig == null) {
            finishWithError("unavailable", "wifi config is null!")
            return
        }
        val list = wifiManager.configuredNetworks
        var netId = wifiManager.addNetwork(wifiConfig)
        for (i in list) {
            if (i.SSID != null && i.SSID == "\"" + ssid + "\"") {
                netId = i.networkId
                break
            }
        }

        if (netId == -1) {
            result?.success(0)
            clearMethodCallAndResult()
        } else {
            // support Android O
            // https://stackoverflow.com/questions/50462987/android-o-wifimanager-enablenetwork-cannot-work
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                wifiManager.enableNetwork(netId, true)
                wifiManager.reconnect()
                result?.success(1)
                clearMethodCallAndResult()
            } else {
                networkReceiver.connect(netId)
            }
        }
    }

    private fun createWifiConfig(ssid: String?, Password: String?): WifiConfiguration {


        val conf = WifiConfiguration()
        conf.SSID = "\"" + ssid + "\""
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
        return conf
        /* val config = WifiConfiguration()
         config.SSID = "\"" + ssid + "\""
         config.allowedAuthAlgorithms.clear()
         config.allowedGroupCiphers.clear()
         config.allowedKeyManagement.clear()
         config.allowedPairwiseCiphers.clear()
         config.allowedProtocols.clear()
         val tempConfig = isExist(wifiManager, ssid)
         if (tempConfig != null) {
             wifiManager.removeNetwork(tempConfig.networkId)
         }

         if (Password.isNullOrEmpty()) {
             config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
         } else {
             config.preSharedKey = "\"" + Password + "\""
             config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN)
             config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP)
             config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK)
             config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP)
             config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP)
             config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP)
         }
         config.hiddenSSID = true
         config.status = WifiConfiguration.Status.ENABLED
        return config*/
    }

    private fun isExist(wifiManager: WifiManager, ssid: String?): WifiConfiguration? {
        val existingConfigs = wifiManager.configuredNetworks
        if (existingConfigs != null) {
            for (existingConfig in existingConfigs) {
                if (existingConfig.SSID == "\"" + ssid + "\"") {
                    return existingConfig
                }
            }
        }
        return null
    }

    private fun setPendingMethodCallAndResult(methodCall: MethodCall, result: MethodChannel.Result): Boolean {
        if (this.result != null) {
            return false
        }
        this.methodCall = methodCall
        this.result = result
        return true
    }

    private fun finishWithAlreadyActiveError() {
        finishWithError("already_active", "wifi is already active")
    }

    private fun finishWithError(errorCode: String, errorMessage: String) {
        result?.error(errorCode, errorMessage, null)
        clearMethodCallAndResult()
    }

    private fun clearMethodCallAndResult() {
        methodCall = null
        result = null
    }

    inner class NetworkChangeReceiver : BroadcastReceiver() {
        private var netId: Int = 0
        private var willLink = false


        override fun onReceive(context: Context, intent: Intent) {
            if (willLink) {
                wifiManager.enableNetwork(netId, true)
                wifiManager.reconnect()
                result!!.success(1)
                willLink = false
                clearMethodCallAndResult()
            }
        }

        fun connect(netId: Int) {
            this.netId = netId
            if (wifiManager.connectionInfo?.ipAddress != 0) {
                willLink = true
            }
            wifiManager.disconnect()

            if (wifiManager.connectionInfo?.ipAddress == 0) {
                wifiManager.enableNetwork(netId, true)
                wifiManager.reconnect()
                result!!.success(1)
                clearMethodCallAndResult()
            }
        }
    }


    fun startRouting(methodCall: MethodCall, result: MethodChannel.Result) {
        val ssid = methodCall.argument<String>("ssid")
        routeNetworkRequestsThroughWifi(ssid!!, result)

    }

    fun stopRouting(methodCall: MethodCall, result: MethodChannel.Result) {
        if (releaseNetworkRoute()) {
            result.success(1)
        } else {
            result.success(0)
        }
    }

    /**
     * This method sets a network callback that is listening for network changes and once is
     * connected to the desired WiFi network with the given SSID it will bind to that network.
     *
     * Note: requires android.permission.INTERNET and android.permission.CHANGE_NETWORK_STATE in
     * the manifest.
     *
     * @param ssid The name of the WiFi network you want to route your requests
     */
    /**
     * This method sets a network callback that is listening for network changes and once is
     * connected to the desired WiFi network with the given SSID it will bind to that network.
     *
     * Note: requires android.permission.INTERNET and android.permission.CHANGE_NETWORK_STATE in
     * the manifest.
     *
     * @param ssid The name of the WiFi network you want to route your requests
     */
    private fun routeNetworkRequestsThroughWifi(ssid: String, result: MethodChannel.Result) {

        // ensure prior network callback is invalidated
        unregisterNetworkCallback(mNetworkCallback)

        // new NetworkRequest with WiFi transport type
        val request = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()

        // network callback to listen for network changes
        mNetworkCallback = object : ConnectivityManager.NetworkCallback() {

            // on new network ready to use
            override fun onAvailable(network: Network) {

                activity.runOnUiThread {
                    if (getNetworkSsid().equals(ssid, ignoreCase = false)) {
                        releaseNetworkRoute()
                        createNetworkRoute(network)
                        result.success(1)
                    } else {
                        releaseNetworkRoute()
                        result.success(0)
                    }
                }
            }
        }
        connectivityManager.requestNetwork(request, mNetworkCallback)
    }

    private fun unregisterNetworkCallback(networkCallback: ConnectivityManager.NetworkCallback?) {
        if (networkCallback != null) {
            try {
                connectivityManager.unregisterNetworkCallback(networkCallback)

            } catch (ignore: Exception) {
            } finally {
                mNetworkCallback = null
            }
        }
    }

    private fun createNetworkRoute(network: Network): Boolean? {
        var processBoundToNetwork: Boolean? = false
        when {
            // 23 = Marshmallow
            Build.VERSION.SDK_INT >= 23 -> {
                processBoundToNetwork = connectivityManager.bindProcessToNetwork(network)
            }

            // 21..22 = Lollipop
            Build.VERSION.SDK_INT in 21..22 -> {
                processBoundToNetwork = ConnectivityManager.setProcessDefaultNetwork(network)
            }
        }
        return processBoundToNetwork
    }

    private fun releaseNetworkRoute(): Boolean {
        var processBoundToNetwork: Boolean = false
        when {
            // 23 = Marshmallow
            Build.VERSION.SDK_INT >= 23 -> {
                processBoundToNetwork = connectivityManager.bindProcessToNetwork(null)
            }

            // 21..22 = Lollipop
            Build.VERSION.SDK_INT in 21..22 -> {
                processBoundToNetwork = ConnectivityManager.setProcessDefaultNetwork(null)
            }
        }
        unregisterNetworkCallback(mNetworkCallback)
        return processBoundToNetwork
    }

    private fun getNetworkSsid(): String {
        // WiFiManager must use application context (not activity context) otherwise a memory leak can occur
        val wifiInfo: WifiInfo? = wifiManager.connectionInfo
        if (wifiInfo?.supplicantState == SupplicantState.COMPLETED) {
            return wifiInfo.ssid.removeSurrounding("\"")
        }
        return ""
    }

}