import 'dart:async';

import 'package:flutter/services.dart';

enum WifiState { error, success, already }

class WifiManager {
  static const MethodChannel _channel =
      const MethodChannel('wifi_manager');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get ssid async {
    return await _channel.invokeMethod('ssid');
  }


  static Future<int> startRouting(String ssid) async {
     final Map<String, dynamic> params = {
      'ssid': ssid
    };
    return await _channel.invokeMethod('startRouting',params);
  }

  static Future<int> stopRouting() async {
    return await _channel.invokeMethod('stopRouting');
  }

 static Future<WifiState> connection(String ssid, String password) async {
    final Map<String, dynamic> params = {
      'ssid': ssid,
      'password': password,
    };
    int state = await _channel.invokeMethod('connection', params);
    switch (state) {
      case 0:
        return WifiState.error;
      case 1:
        return WifiState.success;
      case 2:
        return WifiState.already;
      default:
        return WifiState.error;
    }
  }
}
