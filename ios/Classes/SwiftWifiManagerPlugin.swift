import Flutter
import UIKit

import Foundation
import SystemConfiguration.CaptiveNetwork
import NetworkExtension

public class SwiftWifiManagerPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "wifi_manager", binaryMessenger: registrar.messenger())
        let instance = SwiftWifiManagerPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if(call.method=="ssid"){
            let wifiName = fetchSSIDInfo()
            result(wifiName)
        } else if(call.method=="connection"){
            connect(call,result:result)
        }else if(call.method=="startRouting" || call.method=="stopRouting"){
            result(1)
        }
        else{
            result("iOS " + UIDevice.current.systemVersion)
        }
    }
    
    func fetchSSIDInfo() -> String {
        var currentSSID = ""
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if let interfaceData = unsafeInterfaceData as? [String: AnyObject] {
                    currentSSID = interfaceData["SSID"] as! String
                    debugPrint("ssid=\(currentSSID)")
                }
            }
        }
        return currentSSID
    }
    
    func connect(_ call: FlutterMethodCall, result: @escaping FlutterResult){
        if #available(iOS 11.0, *){
            let argsMap: [String: String] = call.arguments as! [String : String]
            let ssid = argsMap["ssid"]
            debugPrint("connect to ssid=\(ssid)")
            let hotspotConfig = NEHotspotConfiguration(ssid: ssid!)
            NEHotspotConfigurationManager.shared.apply(hotspotConfig) {[unowned self] (error) in
                
                if let error = error {
                    result(0)
                }
                else {
                    result(1)
                }
            }
        }
    }
}
